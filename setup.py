#!/usr/bin/env python

from distutils.core import setup


def readme():
    with open('README.md') as f:
        return f.read()


setup(name='Distutils',
      version='1.0',
      description=readme(),
      author='Medha Bhat',
      packages=['analyse'], requires=['requests', 'pandas']
      )
