import urllib
import requests
import zipfile
import pandas as pd
import re
import time
import os
from analyse.getresponse import get_response
from threading import Thread, Lock
from analyse.topelements import print_top_elements

# defines number of rows
n = 1000
header_dict = {}
lock = Lock()


def analyse_url(start):
    """ reads row from zip file in increment of 50 and calls get_response """
    print(start)
    zf = zipfile.ZipFile('alexa_top_site.zip')
    df = pd.read_csv(zf.open('top-1m.csv'), header=None, usecols=[1], skiprows=int(start), nrows=50)
    for data in df.values:

        data = re.findall(r"[a-zA-Z0-9]+.[a-zA-Z0-9]+", data[0])
        prefix = "https://www."
        url = prefix + data[0]

        try:
            get_response(url, header_dict, lock)
        except requests.exceptions.Timeout as e:
            print('Failed to connect to container ({})'.format(e))

        except requests.exceptions.TooManyRedirects:
            print("Bad URL")

        except requests.exceptions.RequestException as e:
            print(e)


def main():
    """starts 20 threads and calls analyse_url"""
    start_time = time.perf_counter()
    if os.path.isfile('alexa_top_site.zip'):
        os.remove('alexa_top_site.zip')

    link = "http://s3.amazonaws.com/alexa-static/top-1m.csv.zip"
    urllib.request.urlretrieve(link, 'alexa_top_site.zip')

    wait = []
    for i in range(20):
        start = (i * n) / 20
        t = Thread(target=analyse_url, args=(start,))
        wait.append(t)
        t.start()
    for t in wait:
        t.join()
    print_top_elements(header_dict, n)
    print("Total Time :", round((time.perf_counter() - start_time) / 60, 2), " min")


if __name__ == "__main__":
    main()
