"""Get http response of URL and add header to dict"""

import requests
from requests.adapters import HTTPAdapter


def get_response(url, header_dict, lock):
    s = requests.Session()
    s.mount('https://', HTTPAdapter(max_retries=3))
    response = s.get(url, timeout=(5, 10))
    v = response.headers.keys()
    for val in v:
        lock.acquire()
        if val in header_dict:
            header_dict[val] = header_dict.get(val) + 1
        else:
            header_dict[val] = 1
        lock.release()
    return header_dict
