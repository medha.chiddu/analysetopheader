""" Print top 10 headers from dict """

from itertools import islice


def print_top_elements(header_dict, n):
    if n > 0:
        top_header = dict(islice(sorted(header_dict.items(), key=lambda kv: kv[1], reverse=True), 10))
        for key, value in top_header.items():
            print(key, " -> ", value * 100 / n, "%")
